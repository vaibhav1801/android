package io.krumbs.sdk.starter;

/**
 * Created by azriel on 3/9/17.
 */

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class CustomListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private ArrayList<String> itemname;
    private final Integer[] imgid;
    private ArrayList<String> imgURLs;
    private JSONArray dispInfo;


//    private final JSONObject imgJSON;

    public CustomListAdapter(Activity context, ArrayList<String> itemname, Integer[] imgid) {
        super(context, R.layout.mylist, itemname);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemname = itemname;
        this.imgURLs = new ArrayList<String>();
        this.imgid=imgid;
    }

    public CustomListAdapter(Activity context, ArrayList<String> itemname, ArrayList<String> imgid, JSONArray imgInfo)  {
        super(context, R.layout.mylist, itemname);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemname = itemname;
        this.imgURLs= imgid;
        this.imgid = new Integer[2];
        this.dispInfo = imgInfo;
    }


    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.mylist, null,true);

        String dispText;
        try {
            JSONObject currObj = dispInfo.getJSONObject(position);
            dispText = currObj.getString("intent") +
                    " at "+currObj.getString("location") ;
//                    "At : "+currObj.getString("when")
        } catch (JSONException e) {
            e.printStackTrace();
            dispText = "Data could not be read";
        }

        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);


            new DownloadImageTask((ImageView) rowView.findViewById(R.id.icon))
                    .execute(this.imgURLs.get(position));

//        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
//        TextView extratxt = (TextView) rowView.findViewById(R.id.textView1);

        txtTitle.setText(dispText);
//        imageView.setImageResource(imgid[position]);
//        extratxt.setText("Description "+itemname.get(position));
        return rowView;

    };

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
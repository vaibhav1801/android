package io.krumbs.sdk.starter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import static io.krumbs.sdk.starter.Profile.checkSelected;

/**
 * Created by azriel on 3/12/17.
 */

public class simpleAdapter extends BaseAdapter {
    private ArrayList<String> mListItems;
    private LayoutInflater mInflater;
    private TextView mSelectedItems;
    private int selectedCount = 0;
    private static String firstSelected = "";
    private simpleAdapter.ViewHolder holder;
    private static String selected = "";	//shortened selected values representation
    private static ArrayList<String> selectedTags;

    public static ArrayList<String> getSelectedTags(){
        return selectedTags;
    }

    private static void setSelectedTags(ArrayList<String> tags){
        selectedTags = tags;
    }

    public static String getSelected() {
        return selected;
    }

    public static void setSelected(String selected) {
        simpleAdapter.selected = selected;
    }

    public simpleAdapter(Context context, ArrayList<String> items,
                               TextView tv) {
        mListItems = new ArrayList<String>();
        mListItems.addAll(items);
        mInflater = LayoutInflater.from(context);
        mSelectedItems = tv;
    }

    public simpleAdapter(Context context, ArrayList<String>items){
        mListItems = new ArrayList<String>();
        mListItems.addAll(items);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mListItems.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

//    @Override
//    public Filter getFilter(){
//        return new ArrayAdapter<String>().getFilter();
//
//    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.drop_down_list_row, null);
            holder = new simpleAdapter.ViewHolder();
            holder.tv = (TextView) convertView.findViewById(R.id.DropDownList_SelectOption);
            holder.chkbox = (CheckBox) convertView.findViewById(R.id.DropDownList_checkbox);
            convertView.setTag(holder);
        } else {
            holder = (simpleAdapter.ViewHolder) convertView.getTag();
        }

        holder.tv.setText(mListItems.get(position));

        final int position1 = position;

        //whenever the checkbox is clicked the selected values textview is updated with new selected values
        holder.chkbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setText(position1);
            }
        });

        if(checkSelected[position])
            holder.chkbox.setChecked(true);
        else
            holder.chkbox.setChecked(false);
        return convertView;
    }


    /*
     * Function which updates the selected values display and information(checkSelected[])
     * */
    private void setText(int position1){

        selectedCount = 0;
        if (!checkSelected[position1]) {
            checkSelected[position1] = true;

        } else {
            checkSelected[position1] = false;

        }

        ArrayList<String> selTags = new ArrayList<String>();
        for (int i =0; i<checkSelected.length; i++)
            if(checkSelected[i]) {
                selectedCount++;
                selTags.add(mListItems.get(i));
            }

        setSelectedTags(selTags);


        if (selectedCount == 0) {
            mSelectedItems.setText(R.string.select_string);
            setSelected("");
            setSelectedTags(new ArrayList<String>(mListItems));
        } else if (selectedCount == 1) {
            for (int i = 0; i < checkSelected.length; i++) {
                if (checkSelected[i] == true) {
                    firstSelected = mListItems.get(i);
                    selTags.add(mListItems.get(i));
                    break;
                }
            }
            setSelected(firstSelected);
            mSelectedItems.setText(firstSelected);

        } else if (selectedCount > 1) {
            for (int i = 0; i < checkSelected.length; i++) {
                if (checkSelected[i] == true) {
                    firstSelected = mListItems.get(i);
                    break;
                }
            }
            mSelectedItems.setText(firstSelected + " & "+ (selectedCount - 1) + " more");
            setSelected(firstSelected + " & "+ (selectedCount - 1) + " more");
        }
    }

    private class ViewHolder {
        TextView tv;
        CheckBox chkbox;
    }
}

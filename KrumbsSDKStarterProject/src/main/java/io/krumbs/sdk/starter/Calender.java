package io.krumbs.sdk.starter;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.*;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.common.api.GoogleApiClient;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.LabelFormatter;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.Series;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import java.util.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.NumberFormat;
import android.content.Intent;
import android.graphics.Color;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;

import com.jjoe64.graphview.DefaultLabelFormatter;
import java.util.concurrent.TimeUnit;


public class Calender extends AppCompatActivity {


    String records;
    JSONArray imgURLs;
    HashMap hm = new HashMap();
    FrameLayout progressBarHolder;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_calender);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setTitle(R.string.app_name);
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        }
       setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
//        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        progressBarHolder = (FrameLayout) findViewById(R.id.progressBarHolder);

        imgURLs = new JSONArray();
        GraphView graph = (GraphView) findViewById(R.id.graph1);
        DataPoint[] dataPoints = new DataPoint[6];
        for (int i = 0; i < 6; i++) {
            DataPoint d = new DataPoint(i, i + 1);
            dataPoints[i] = d;
        }
        long start_time = getIntent().getExtras().getLong("START_DATE");
        long end_time = getIntent().getExtras().getLong("END_DATE");
        long size = (end_time - start_time) / 86400000;

        String format = "weekly";

        //String jsonStr = "{\"events\": [ {\"start_time\":\"6\",\"score\":\"2\"} , {\"start_time\":\"6\",\"score\":\"3\"}] }";
        try {
            dataPoints = getAllScores(start_time, end_time, (int) size);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception0");
        }

        initGraph(start_time, graph, dataPoints);


    }

    public void popHashMap() {
        hm.put("Happy", 4.0);
        hm.put("Cool", 3.0);
        hm.put("Peaceful", 4.0);
        hm.put("Love", 5.0);
        hm.put("Blush", 4.0);
        hm.put("Work", 1.0);
        hm.put("Chill", 2.0);
        hm.put("Angry", 1.0);
        hm.put("Excited", 5.0);
        hm.put("Blessed", 4.0);
        hm.put("Drinks", 3.0);
        return;
    }

    public JSONObject getJsonFromServer() throws MalformedURLException, IOException, JSONException { //get situations json from the server
        JSONObject jsonObj;

        //URL url = new URL("http://10.0.2.2:3000/reports");
        URL url = new URL("https://api.krumbs.io/v2/situations");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        //urlConnection.setRequestProperty("Krumbs-ApplicationID", "quAQOM3P59WECSyvdLgSqHNVCN6nmuHy-ssGn3SD7Qk=");
        //urlConnection.setRequestProperty("Krumbs-ClientKey", "R2H6NCJsqD7hvmoqXfAxAr7D0mZCbm3pElLXH-rHGA8=");
        urlConnection.setRequestProperty("Krumbs-ApplicationID", "_BU-CEgxh4JFxOTEuWPpI7Is_JTqZTpFltC-bol5TZg=");
        urlConnection.setRequestProperty("Krumbs-ClientKey", "ms1-T7OAB2gMjcaUQBKrPJd2D4p7PYE4dQRUnjyVpDU=");
        int responseCode = urlConnection.getResponseCode();
        System.out.println(responseCode);
        InputStream is = new BufferedInputStream(urlConnection.getInputStream());
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder(is.available());

        String line;
        while ((line = reader.readLine()) != null) {
            total.append(line).append('\n');
        }

        String output = total.toString();
        System.out.println("Output:" + output);
        records = output;
        jsonObj = new JSONObject(output);

        return jsonObj;
    }

    long getTimefromString(String time) throws ParseException { //convert string time to unixtime
        String s = "";
        for (int i = 0; i < time.length(); i++) {
            if (time.charAt(i) == 'T') {
                break;
            }
            s += time.charAt(i);
        }
        DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
        Date date = (Date) formatter.parse(s);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int mon = cal.MONTH;
        cal.add(Calendar.MONTH, mon);
        date = cal.getTime();
        long tm = date.getTime();
        //System.out.println("Before " + time);
        //System.out.println("After " + convert(tm));
        return tm;
    }

    public void getImageURLs(JSONObject obj, JSONObject weather) throws JSONException { //Takes json object and obtains all imageURLs from it
        JSONObject media_source = obj.getJSONObject("media_source");
        JSONObject where = obj.getJSONObject("where");
        JSONArray arr = where.getJSONArray("revgeo_places");
        JSONObject places = arr.getJSONObject(0);
        JSONArray why = obj.getJSONArray("why");
        JSONObject js = why.getJSONObject(0);
        String intent_name = js.getString("intent_name");

        String url = media_source.getString("default_src");
        JSONObject capsule = new JSONObject();

        capsule.put("place", places.getString("name"));
        String w = weather.getString("type");
        capsule.put("weather", w);
        capsule.put("url", url);
        capsule.put("intent", intent_name);
        //System.out.println("Capsule "+capsule.toString());
        imgURLs.put(capsule);
    }

    public DataPoint[] getAllScores(long start_time,long end_time,int size)  throws org.json.JSONException, IOException
    {   //Get happiness scores for concerned intervals

        popHashMap();

        DataPoint[] dataPoints = new DataPoint[size];
        ArrayList scores = new ArrayList();
        ArrayList count = new ArrayList();
        JSONObject jsonObj;
        JSONArray events;
        long interval = (end_time - start_time)/size;
        for(int i = 0;i<size;i++)
        {
            scores.add(0.0);
            count.add(0.0001);


        }
        try{ jsonObj = getJsonFromServer();
            events = jsonObj.getJSONArray("reports");}
        catch( Exception e)
        {
            for(int i = 0;i<size;i++) { dataPoints[i] = new DataPoint(start_time + interval*i,0); }
            return dataPoints;
        }



        //System.out.println("Interval "+interval);
        for(int i = 0;i<events.length();i++)
        {
            JSONObject o = events.getJSONObject(i);
            JSONArray media = o.getJSONArray("media");
            for(int j = 0;j<media.length();j++) {

                JSONObject c = media.getJSONObject(j);
                JSONObject when = c.getJSONObject("when");
                String time = when.getString("start_time");
                long timestamp = 0;
                try { timestamp = getTimefromString(time);}
                catch(Exception e) { }
                System.out.println("timestamp "+timestamp);
                System.out.println(start_time);
                System.out.println(end_time);
                if (timestamp < start_time || timestamp > end_time) {

                    continue;
                }
                System.out.println("No matching time");

                try { JSONObject environment = o.getJSONObject("environment_measure");
                    JSONObject weather = environment.getJSONObject("weather");getImageURLs(c,weather); }
                catch(Exception e) { continue; }

                //System.out.println("Check 1");
                JSONArray why;
                try { why = c.getJSONArray("why"); }
                catch(Exception e) { continue;}

                //System.out.println("Check 2");
                JSONObject happy = why.getJSONObject(0);
                String name = happy.getString("intent_name");
                double happiness = 3.0;
                if (hm.containsKey(name))
                {
                    happiness = Double.parseDouble(hm.get(name).toString());
                }
                System.out.println("Happy "+happiness);
                int index = (int) (((timestamp - start_time) * size) / (end_time - start_time));
                System.out.println(index);
                scores.set(index, (Double) scores.get(index) + happiness);
                count.set(index, (Double) count.get(index) + 1);
            }

        }

        for(int i = 0;i<size;i++)
        {
            dataPoints[i] = new DataPoint(start_time + interval*i,(double)scores.get(i)/(double)count.get(i));
            System.out.println("The datapoint is "+(start_time + interval*i)+" "+dataPoints[i].getY());
        }
        return dataPoints;
    }

    public String convert(long unixtime) { //convert unixtime to date
        Long utime = (long) unixtime;
        Date date = new Date(utime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(date);
        //System.out.println(formattedDate);
        return formattedDate;
    }


    protected void setViewport(long start_time, GraphView graph, Series series) {
        //make the viewport
        graph.getViewport().setXAxisBoundsManual(true);
        Date d1 = new Date(start_time);
        Date d2 = new Date(start_time + 3 * 86400000L);
        graph.getViewport().setMinX(d1.getTime());
        graph.getViewport().setMaxX(d2.getTime());
        graph.getViewport().setScalable(true);
        graph.getViewport().setScrollable(true);

    }

    public void initGraph(long start_time, GraphView graph, DataPoint[] dataPoints) {
        //initializes and draws the graph

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dataPoints);
        String[] dates = new String[dataPoints.length];
        for (int i = 0; i < dataPoints.length; i++) {
            dates[i] = convert((long) dataPoints[i].getX());

        }
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        //staticLabelsFormatter.setHorizontalLabels(dates);

        series.setDrawBackground(true);
        //series.setColor(Color.argb(0xFF, 0x8e, 0x00, 0xe8));
        //series.setColor(R.color.logo2color);
        series.setColor(Color.BLUE);
        series.setBackgroundColor(Color.argb(0x3F, 0x47, 0x2c, 0x17));
        series.setThickness(6);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(15f);
        graph.addSeries(series);
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return convert((long) value);
                } else {
                    // show currency for y values
                    return String.format("%.2f", value);
                }
            }
        });
        //graph.getGridLabelRenderer().setNumHorizontalLabels(3);
        graph.getViewport().setScrollable(true);
        graph.getGridLabelRenderer().setHumanRounding(false);
        setViewport(start_time, graph, series);
        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(getApplicationContext(), "Series1: On Data Point clicked: " + dataPoint, Toast.LENGTH_SHORT).show();

            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_graph, menu);

        menu.findItem(R.id.last_day).setChecked(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        item.setChecked(true);
        new MyTask().execute();
        Intent i = new Intent(Calender.this, Profile.class);
        records = imgURLs.toString();
        System.out.println("Output of activity is "+records+"\n\n");
        i.putExtra("RECORDS", records);

        startActivity(i);
        overridePendingTransition(0, 0);
        return true;
    }
    private class MyTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBarHolder.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBarHolder.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                for (int i = 0; i < 5; i++) {
                    TimeUnit.SECONDS.sleep(1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
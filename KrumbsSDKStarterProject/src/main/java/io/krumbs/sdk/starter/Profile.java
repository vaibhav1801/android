package io.krumbs.sdk.starter;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar.LayoutParams;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ListActivity;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;


import clarifai2.Func0;
import clarifai2.api.ClarifaiBuilder;
import clarifai2.api.ClarifaiClient;
import clarifai2.api.ClarifaiResponse;
import clarifai2.api.request.ClarifaiPaginatedRequest;
import clarifai2.api.request.ClarifaiRequest;
import clarifai2.exception.ClarifaiException;
import clarifai2.internal.InternalUtil;
import okhttp3.OkHttpClient;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import clarifai2.dto.input.ClarifaiInput;
import clarifai2.dto.input.image.ClarifaiImage;
import clarifai2.dto.ClarifaiStatus;
import clarifai2.dto.input.SearchHit;
import clarifai2.dto.input.image.ClarifaiImage;
import clarifai2.dto.input.image.Crop;
import clarifai2.dto.model.ConceptModel;
import clarifai2.dto.model.DefaultModels;
import clarifai2.dto.model.output.ClarifaiOutput;
import clarifai2.dto.model.Model;
import clarifai2.dto.model.ModelTrainingStatus;
import clarifai2.dto.model.ModelVersion;
import clarifai2.dto.model.output_info.ConceptOutputInfo;
import clarifai2.dto.prediction.Concept;

import static android.R.attr.tag;
import static java.lang.reflect.Modifier.isPublic;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;



public class Profile extends AppCompatActivity {

//    private TextView text1;
    //image url = http://media.krumbs.io/0ad9c3d7-615a-4557-8bd3-e6abbe77d7dd.jpeg
    private boolean expanded;
    private PopupWindow pw;
    private ArrayList<String> items;
    public static boolean [] checkSelected;
    public static ArrayList<String> selectedTags;

    private ArrayList<String> imgURL;
    private ArrayList<String> clarifaiTags;

    private JSONArray intentArray;
    private JSONArray dispInfo;


    public JSONArray extractConcepts(String concepts)
    {
        JSONArray results = new JSONArray();
        String s = "";
        boolean flag = false;
        for(int  i =0;i<concepts.length();i++)
        {
            if(i>80 && concepts.charAt(i)=='=' && concepts.charAt(i-1)=='e' && concepts.charAt(i-2)=='m')
            {
               flag = true;
                s = "";
                continue;
            }
            if(flag)
            {
               if(concepts.charAt(i)!=',' ) { s+=concepts.charAt(i); }
                else { flag = false; results.put(s);}
            }

        }
        //System.out.println( results.toString());
        return results;

    }

    public JSONArray getClarifaiConcepts(String baseURL)
    { //Get concepts as JSONArray from url

        JSONArray conceptarr = new JSONArray();
        System.out.println(baseURL);
        String client_id = "EN0F9-eMoHG4JxBZV2Vn6zc3-jHBpk2IG0UhGxrV";
        String client_secret = "0y9UQ1UCaX-m1HyT89Vh1bqfY2GiotnN6QK3IBAR";

        /*ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(Profile.this);
        progressDoalog.show();*/
        //Create a new client
        final ClarifaiClient client  = new ClarifaiBuilder(client_id, client_secret)
                .client(new OkHttpClient()) // OPTIONAL. Allows customization of OkHttp by the user
                .buildSync();
        final List<ClarifaiOutput<Concept>> predictionResults =
                client.getDefaultModels().generalModel() // You can also do Clarifai.getModelByID("id") to get custom models
                        .predict()
                        .withInputs(
                                ClarifaiInput.forImage(ClarifaiImage.of(baseURL))
                        )
                        .executeSync() // optionally, pass a ClarifaiClient parameter to override the default client instance with another one
                        .get();
        //System.out.println("Size of list is " + predictionResults.size());

        //progressDoalog.hide();
        String concepts = predictionResults.get(0).toString();
        conceptarr = extractConcepts(concepts);
        return conceptarr;
    }

    public JSONArray doConcepts(String record) throws JSONException
    { //Get all concepts from the string of records.
        JSONArray records = new JSONArray(record);
        JSONArray results = new JSONArray();
        //JSONObject capsule = new JSONObject();
        for(int i = 0;i<records.length();i++)
        {
            JSONObject capsule = new JSONObject();

            JSONObject obj = records.getJSONObject(i);
            String baseURL = obj.getString("url");
            JSONArray res = getClarifaiConcepts(baseURL);
            String location = obj.getString("place");
            String intent = obj.getString("intent");

            capsule.put("url",baseURL);
            capsule.put("concepts",res);
            capsule.put("location", location);
            capsule.put("intent", intent);

            results.put(capsule);
        }
        System.out.println(results.toString());
        return results;
    }


        ListView list;
        CustomListAdapter customAdapter;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_profile);
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_SENSOR);

//            selectedTags = new ArrayList<String>(Arrays.asList(itemname));
            Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
            if (toolbar != null) {
                setSupportActionBar(toolbar);
                toolbar.setTitle(R.string.app_name);
                toolbar.setTitleTextColor(getResources().getColor(R.color.white));

            }
            imgURL = new ArrayList<String>();
            clarifaiTags = new ArrayList<String>();

            String imgURLs = getIntent().getExtras().getString("RECORDS");

            System.out.println(imgURLs);

            JSONArray imgTags;
//            JSONArray infoDisplay = new JSONArray();
            HashSet<String> uniqueTags = new HashSet<String>();
            try {
                imgTags = doConcepts(imgURLs);
                intentArray = imgTags;
                dispInfo = new JSONArray();
                for(int i =0;i < imgTags.length(); i++){
                    JSONArray tags = imgTags.getJSONObject(i).getJSONArray("concepts");
                    dispInfo.put(imgTags.getJSONObject(i));
                    imgURL.add(imgTags.getJSONObject(i).getString("url"));
                    clarifaiTags.add(imgTags.getJSONObject(i).getJSONArray("concepts").toString());
                    for(int j= 0; j< tags.length() && j<5; j++){
                        uniqueTags.add(tags.getString(j));
                    }
                }
                System.out.println(imgTags.toString());
                System.out.println(uniqueTags.toString());


            } catch (JSONException e) {
                e.printStackTrace();
                System.exit(1);
            }

            //drop down preparation
            expanded = false;
          //  final ArrayList items = new ArrayList(uniqueTags);
            //                    new ArrayList(Arrays.asList(itemname));
            items = new ArrayList(uniqueTags);
//            final JSONArray allTags = new JSONArray(uniqueTags);

            checkSelected = new boolean[items.size()];
            for (int i = 0; i < checkSelected.length; i++) {
                checkSelected[i] = false;
            }

            final TextView tv = (TextView) findViewById(R.id.DropDownList_SelectBox);
            LinearLayout dropdown = (LinearLayout) findViewById(R.id.dropdown);
            dropdown.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    initiatePopUp(items,tv);
                }
            });
            tv.setOnClickListener(new OnClickListener() {

                // do search in this listener
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if(!expanded){
                        //display all selected values
                        // call with all the tags selected
                        String selected = "";
                        int flag = 0;
                        for (int i = 0; i < items.size(); i++) {
                            if (checkSelected[i] == true) {
                                selected += items.get(i);
                                selected += ", ";
                                flag = 1;
                            }
                        }
                        if(flag==1)
                            tv.setText(selected);
                        expanded =true;
                    }
                    else{
                        // display shortened representation of selected values
                        // call with only the selected tags from the adapter
                        String text = simpleAdapter.getSelected();
                        if(text != "")
                            tv.setText(text);
                        expanded = false;
                    }
                }
            });

            tv.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    //change the content of the textview here
                    ArrayList<String> newTags = simpleAdapter.getSelectedTags();
                    if(newTags.size() == 0){
                        newTags = new ArrayList<String>(items);
                    }
                    JSONArray selectedTags = new JSONArray();
                    for(int i =0; i<newTags.size(); i++){
                        selectedTags.put(newTags.get(i));
                    }
                    try {
                        refreshText(selectedTags, intentArray);
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                        System.exit(1);
                    }
                    customAdapter=new CustomListAdapter(Profile.this, Profile.this.clarifaiTags, Profile.this.imgURL, Profile.this.dispInfo);
                    list=(ListView)findViewById(R.id.list);
                    list.setAdapter(customAdapter);
                    runOnUiThread(new Runnable(){
                        public void run(){
                            customAdapter.notifyDataSetChanged();
                        }
                    });

                }
            });
            Button createButton = (Button)findViewById(R.id.DropDownList_create);
            createButton.setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    initiatePopUp(items,tv);
                }
            });


            customAdapter=new CustomListAdapter(this, clarifaiTags, imgURL, dispInfo);
            list=(ListView)findViewById(R.id.list);
            list.setAdapter(customAdapter);
//            runOnUiThread(new Runnable(){
//                public void run(){
//                    customAdapter.notifyDataSetChanged();
//                }
//            });
            list.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                ImageView imgView=(ImageView)view.findViewById(R.id.icon);
                    loadPhoto(imgView);
                }
            });
        }

    private void loadPhoto(ImageView imageView) {

        ImageView tempImageView = imageView;


        AlertDialog.Builder imageDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.custom_fullimage_dialog,
                (ViewGroup) findViewById(R.id.layout_root));
        ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
        image.setImageDrawable(tempImageView.getDrawable());
        imageDialog.setView(layout);
        imageDialog.setPositiveButton(getResources().getString(R.string.ok_button), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });


        imageDialog.create();
        imageDialog.show();
    }

    //to populate the popup window
    private void initiatePopUp(ArrayList<String> items, TextView tv){

        expanded = true;
        final FragmentManager fm=getFragmentManager();
        final dropDownFragment p=new dropDownFragment();
        p.setItems(items);
        p.setResultView(tv);

        p.show(fm, "Tags");

    }

    public void refreshText(JSONArray tags, JSONArray concepts) throws JSONException{

        JSONArray refreshedList = searchImages(tags, concepts);
        ArrayList<String> urlArr = new ArrayList<String>();
        ArrayList<String> newTags = new ArrayList<String>();

        for(int i = 0; i<refreshedList.length(); i++)
        {
            urlArr.add(refreshedList.getJSONObject(i).getString("url"));
            newTags.add(refreshedList.getJSONObject(i).getJSONArray("concepts").toString());
        }

        clarifaiTags = newTags;
        imgURL = urlArr;
        dispInfo = refreshedList;
    }

    public JSONArray searchImages(JSONArray tags, JSONArray concepts) throws JSONException
    {//Gets a list of tags as input and returns a JSON array for matching urls
        JSONArray results = new JSONArray();
        HashSet<String> tagSet = new HashSet<String>();
        for(int i =0;i < tags.length(); i++)
            tagSet.add(tags.getString(i));
        for(int i = 0;i<concepts.length();i++)
        {
            JSONArray concept_list = concepts.getJSONObject(i).getJSONArray("concepts");
            String url = concepts.getJSONObject(i).getString("url");
            JSONObject img = concepts.getJSONObject(i);
            int count = 0;
//            for(int j = 0;j<tags.length();j++)
//            {
//                String tag = tags.getString(j);
                for(int k = 0;k<concept_list.length();k++)
                {
                    String concept = concept_list.getString(k);
//                    if(concept.equals(tag)){
                    if(tagSet.contains(concept)){
                        results.put(img);
//                        count++;
                        break;}

                }
//            }
//            if(count >= tags.length()) { results.put(url);}
        }
        return results;

    }

}
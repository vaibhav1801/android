package io.krumbs.sdk.starter;

import android.app.DatePickerDialog;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;

import android.content.Intent;

import java.text.DateFormat;

import static io.krumbs.sdk.starter.R.style.AppThemeCustomOverflow;
//https://github.com/appsthatmatter/GraphView-Demos/blob/master/app/src/main/java/com/jjoe64/graphview_demos/examples/MultipleBarGraph.java
//http://www.android-graphview.org/tap-listener-on-data/



public class CalenderView extends AppCompatActivity {

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView startdateView, enddateView,displayStartDate,displayEndDate;
    private int year, month, day;
    DatePickerDialog.OnDateSetListener date1,date2;


    private long convert(String date1,boolean end) throws java.text.ParseException
    {
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date date = (Date) formatter.parse(date1);
        //System.out.println(date.getTime());
        if (end == true) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int mon = cal.DATE;
            cal.add(Calendar.DATE, 1);
            date = cal.getTime();
        }
        return date.getTime();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender_view);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setTitle(R.string.app_name);
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        }
        calendar = Calendar.getInstance();
        startdateView = (TextView) findViewById(R.id.date1);
        enddateView = (TextView) findViewById(R.id.date2);
        displayStartDate= (TextView)findViewById(R.id.displayStartDate);
        displayEndDate= (TextView)findViewById(R.id.displayEndDate);
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("M/d/yyyy");
        String formattedDate = df.format(c.getTime());
        displayStartDate.setText(formattedDate);
        displayEndDate.setText(formattedDate);
        date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                //calendar.set(Calendar.YEAR, year);
                //calendar.set(Calendar.MONTH, monthOfYear);
                //calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                showDate(displayStartDate,year,monthOfYear,dayOfMonth);
            }

        };

        date2 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                showDate(displayEndDate,year,monthOfYear,dayOfMonth);
            }

        };

        ImageButton button1= (ImageButton) findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(CalenderView.this, R.style.DialogTheme,date1, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        ImageButton button2= (ImageButton) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(CalenderView.this, R.style.DialogTheme,date2, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        ImageButton button3= (ImageButton) findViewById(R.id.submit_button);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)  {
                String start = displayStartDate.getText().toString();
                String end = displayEndDate.getText().toString();
                long st_time = 1372219820 * 1000L;
                long en_time = 1372219820 * 1000L;
                try {
                    st_time = convert(start,false);
                    en_time = convert(end,true);
                }
                catch (Exception e){e.printStackTrace(); System.out.println("Exception0"); }
                Intent i = new Intent(CalenderView.this, Calender.class);
                i.putExtra("START_DATE", st_time);
                i.putExtra("END_DATE", en_time);
                startActivity(i);
            }
        });

    }

    private void showDate(TextView text, int year, int month, int day) {
        text.setText(new StringBuilder().append(month+1).append("/").
                append(day).append("/").append(year));
    }



}
